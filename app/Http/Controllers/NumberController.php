<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class NumberController extends Controller
{
    public function insert_numbers(Request $request)
    {
        $input = $request->numbers;
        $temp = 0;
        $number_array = array_map('intval', explode(' ', $input));
        $index = 0;
        while($index < sizeof($number_array) - 1){
            if($number_array[$index] > $number_array[$index+1])
            {
                $temp = $number_array[$index+1];
                $number_array[$index+1] = $number_array[$index];
                $number_array[$index] = $temp;
                $index = -1;
                
            }
            ++$index;
        }

        $number_array_as_string = implode(' ',$number_array);

        return $number_array_as_string;  
    }
}
